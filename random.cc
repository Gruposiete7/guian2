
#include <iostream>
#include <ctime>
#include <stdio.h>

 
/* function to generate and return random numbers*/

int * getRandom(){

     static int r[10];
     int i;

     /* set the seed*/
    srand ( (unsigned)time(NULL) );

      for (i = 0; i < 10; ++i) {
          r[i] = rand();
          printf("r[%d] = %d\n" , i, r[i]) ;
    }//for
  return r;

}//int 1

/* main fuction to call above defined fuction */
int main () {
     /* apointer to an int*/
  int *p;
  int i;
  p = getRandom();
  for (i = 0; i < 10; i++){
       printf("*(p + %d): %d\n", i, *(p + i));
  }
   return 0; 

}// int 2
